FROM registry.access.redhat.com/ubi9/ubi-minimal:latest AS ookla

RUN microdnf -y install tar gzip && microdnf clean all
RUN mkdir -p /opt/speedtest
ADD https://install.speedtest.net/app/cli/ookla-speedtest-1.2.0-linux-x86_64.tgz /opt/ookla-speedtest.tgz
RUN tar -xf /opt/ookla-speedtest.tgz -C /opt/speedtest

FROM registry.access.redhat.com/ubi9/ubi-minimal:latest

RUN mkdir -p /opt/speedtest
COPY --from=ookla  /opt/speedtest/speedtest /opt/speedtest
RUN ln -s /opt/speedtest/speedtest /usr/local/bin

ENTRYPOINT [ "speedtest" ]
